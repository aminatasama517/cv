# CV

## INTRODUCTION
Ce projet permet de créer un CV sur Hugo .

## INSTRUCTIONS

Il faut d'abord cloner le projet dans votre dossier personnel:
```
git clone https://gitlab.com/aminatasama517/cv.git
```

Entrez dans le fichier "nano data/content.yaml" pour modifier le cv.

Pour modifier l'image il vous suffit de remplacer le fichier "static/img/avatar.png par votre photo.
